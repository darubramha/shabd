![shabd logo](https://docs.google.com/drawings/d/e/2PACX-1vQF8dl7QCwGJeWsIMolz4y0yPHLm3ZulNEgT8zX47qmie4hELSGsEJQvkYYr6x2v63Zm5TMXblc2PnF/pub?w=2686&h=795)

I will fill this up later. For now, these are some scripts that you would want to use.


## Train Script
```sh
python train.py \
--rnn_size=512 \
--num_layers=3 \
--model=lstm \
--seq_length=20 \
--data_dir=<root-of-your-data-directory> \
--dataset=<name-of-your-dataset> \
--experiment_name=<name-of-your-experiment> \
--save_every=100 \
--num_epochs=300 \
--output_keep_prob=0.7 \
--input_keep_prob=0.5
```

If you want to restart training from a previous checkpoint 

```
python train.py \
--rnn_size=512 \
--num_layers=3 \
--model=lstm \
--seq_length=20 \
--data_dir=<root-of-your-data-directory> \
--dataset=<name-of-your-dataset> \
--experiment_name=<name-of-your-experiment> \
--save_every=100 \
--num_epochs=300 \
--output_keep_prob=0.7 \
--input_keep_prob=0.5 \
--data/experiments/<name-of-my-experiment>/checkpoints
```
