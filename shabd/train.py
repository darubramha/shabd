from __future__ import print_function

import argparse
import os
import time

import numpy as np
import spacy
import tensorflow as tf
from six.moves import cPickle
from tensorflow.contrib.tensorboard.plugins import projector

from datasets import dataset_utils as du
from shabd.models.rnn import Model

nlp = spacy.load('en_core_web_md')

def main():
    parser = argparse.ArgumentParser(
                        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data_dir', type=str, default='data/tinyshakespeare',
                        help='data directory containing input.txt')
    parser.add_argument('--dataset', type=str, default='en_hn_corp',
                        help='name of the dataset that you want to load')
    parser.add_argument('--experiment_name', type=str, default='nlm',
                        help='directory to store all experiment related data')
    parser.add_argument('--log_dir', type=str, default='logs',
                        help='directory to store tensorboard logs')
    parser.add_argument('--rnn_size', type=int, default=128,
                        help='size of RNN hidden state')
    parser.add_argument('--num_layers', type=int, default=2,
                        help='number of layers in the RNN')
    parser.add_argument('--model', type=str, default='lstm',
                        help='rnn, gru, lstm, or nas')
    parser.add_argument('--batch_size', type=int, default=50,
                        help='minibatch size')
    parser.add_argument('--seq_length', type=int, default=50,
                        help='RNN sequence length')
    parser.add_argument('--num_epochs', type=int, default=50,
                        help='number of epochs')
    parser.add_argument('--save_every', type=int, default=1000,
                        help='save frequency')
    parser.add_argument('--grad_clip', type=float, default=5.,
                        help='clip gradients at this value')
    parser.add_argument('--learning_rate', type=float, default=0.002,
                        help='learning rate')
    parser.add_argument('--decay_rate', type=float, default=0.97,
                        help='decay rate for rmsprop')
    parser.add_argument('--output_keep_prob', type=float, default=1.0,
                        help='probability of keeping weights in the hidden layer')
    parser.add_argument('--input_keep_prob', type=float, default=1.0,
                        help='probability of keeping weights in the input layer')
    parser.add_argument('--init_from', type=str, default=None,
                        help="""continue training from saved model at this path. Path must contain files saved by previous training process:
                            'config.pkl'        : configuration;
                            'chars_vocab.pkl'   : vocabulary definitions;
                            'checkpoint'        : paths to model file(s) (created by tf).
                                                  Note: this file contains absolute paths, be careful when moving files around;
                            'model.ckpt-*'      : file(s) with model definition (created by tf)
                        """)
    args = parser.parse_args()
    train(args)


def train(args):
    #data_loader = TextLoader(args.data_dir, args.batch_size, args.seq_length)
    dataset, vocab_size, vocab_path, w2i, i2w,\
    metadata_path = du.load_dataset(args.data_dir, args.dataset, nlp,
                                    args.seq_length)
    args.vocab_size = vocab_size
    args.metadata_path = os.path.abspath(metadata_path)
    args.vocab_path = os.path.abspath(vocab_path)


    # check compatibility if training is continued from previously saved model
    if args.init_from is not None:
        # check if all necessary files exist
        assert os.path.isdir(args.init_from)," %s must be a a path" % args.init_from
        assert os.path.isfile(os.path.join(args.init_from,"config.pkl")),"config.pkl file does not exist in path %s"%args.init_from
        assert os.path.isfile(os.path.join(args.init_from,"vocab.pkl")),"vocab.pkl.pkl file does not exist in path %s" % args.init_from
        ckpt = tf.train.get_checkpoint_state(args.init_from)
        assert ckpt, "No checkpoint found"
        assert ckpt.model_checkpoint_path, "No model path found in checkpoint"

        # open old config and check if models are compatible
        with open(os.path.join(args.init_from, 'config.pkl'), 'rb') as f:
            saved_model_args = cPickle.load(f)
        need_be_same = ["model", "rnn_size", "num_layers", "seq_length"]
        for checkme in need_be_same:
            assert vars(saved_model_args)[checkme]==vars(args)[checkme],"Command line argument and saved model disagree on '%s' "%checkme

        # open saved vocab/dict and check if vocabs/dicts are compatible
        with open(os.path.join(args.init_from, 'vocab.pkl'), 'rb') as f:
            config_w2i, config_i2w = cPickle.load(f)
        assert config_w2i==w2i, "Data and loaded model disagree on character " \
                               "set!"
        assert config_i2w==i2w, "Data and loaded model disagree on " \
                                 "dictionary mappings!"

    args.log_dir = os.path.join(args.data_dir, 'experiments',
                                args.experiment_name, 'checkpoints')
    args.eval_log_dir = os.path.join(args.data_dir, 'experiments',
                                args.experiment_name, 'eval_logs')
    args.save_dir = os.path.join(args.data_dir, 'experiments',
                                args.experiment_name, 'checkpoints')
    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)
    if not os.path.exists(args.eval_log_dir):
        os.makedirs(args.eval_log_dir)
    with open(os.path.join(args.save_dir, 'config.pkl'), 'wb') as f:
        cPickle.dump(args, f)
    with open(os.path.join(args.save_dir, 'vocab.pkl'), 'wb') as f:
        cPickle.dump((w2i, i2w), f)

    model = Model(args)

    with tf.Session() as sess:
        # instrument for tensorboard
        summaries = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter(args.log_dir)
        train_writer.add_graph(sess.graph)
        eval_writer = tf.summary.FileWriter(args.eval_log_dir)
        projector.visualize_embeddings(train_writer, model.config)

        eval_writer.add_graph(sess.graph)

        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver(tf.global_variables())

        # restore model
        if args.init_from is not None:
            print('Trying to restore model from previous checkpoint')
            saver.restore(sess, ckpt.model_checkpoint_path)


        dataset.train.open()
        dataset.validation.open()
        train_batches = dataset.train.next_batch(args.batch_size)
        dev_batches = dataset.validation.next_batch(args.batch_size)

        min_validation_loss = 1000000000.0
        for e in range(args.num_epochs):
            sess.run(tf.assign(model.lr,
                               args.learning_rate * (args.decay_rate ** e)))
            state = sess.run(model.initial_state)
            zero_state = state
            for batch in train_batches:
                start = time.time()
                x, y = batch.sources, batch.targets
                feed = {model.input_data: x, model.targets: y,
                        model.input_keep_prob: args.input_keep_prob,
                        model.output_keep_prob: args.output_keep_prob}
                for i, (c, h) in enumerate(model.initial_state):
                    feed[c] = state[i].c
                    feed[h] = state[i].h
                #train_loss, state, _, global_step = sess.run([model.cost,
                #               model.final_state, model.train_op,
                #               model.global_step_increment], feed)

                # instrument for tensorboard
                summ, train_loss, state, _, global_step = sess.run([summaries,
                        model.cost, model.final_state, model.train_op,
                        model.global_step_increment], feed)
                train_writer.add_summary(summ, global_step)

                end = time.time()
                print("Global Step: {}\nEpoch: {}\nTrain Loss: {:.3f}\n"
                      "time/batch = {:.3f}"
                      .format(global_step, e, train_loss, end - start))
                if (global_step) % args.save_every == 0:
                    # save for the last result
                    validation_loss = validate(sess, dev_batches,
                                           dataset.validation, model,
                                           eval_writer, summaries, zero_state)
                    if validation_loss <= min_validation_loss:
                        min_validation_loss = validation_loss
                        checkpoint_path = os.path.join(args.save_dir,
                                                       'model.ckpt')
                        saver.save(sess, checkpoint_path,
                                   global_step=global_step)
                        print("model saved to {}".format(checkpoint_path))

        dataset.train.close()
        dataset.dev.close()
        dataset.test.open()
        test_batches = dataset.test.next_batch(args.batch_size)
        test_loss = test(sess, test_batches, dataset.test, model)
        results_path = os.path.join(args.data_dir, 'experiments',
                                args.experiment_name, 'results.txt')
        with open(results_path, 'w') as rf:
            rf.write('Test Loss: {}'.format(test_loss))

def validate(sess, batch_generator, dataset, model, eval_writer, summaries,
             state):
    print('validating')
    val_losses = []
    for batch in batch_generator:
        if dataset.epochs_completed == 1:
            dataset._epochs_completed = 0
            break
        x, y = batch.sources, batch.targets
        feed = {model.input_data: x, model.targets: y,
                        model.input_keep_prob: 1.0,
                        model.output_keep_prob: 1.0}
        for i, (c, h) in enumerate(model.initial_state):
            feed[c] = state[i].c
            feed[h] = state[i].h

        # instrument for tensorboard
        summ, val_loss, state, eval_step = sess.run([summaries,
                                        model.cost, model.final_state,
                                        model.eval_step_increment], feed)
        val_losses.append(val_loss)
        eval_writer.add_summary(summ, eval_step)
    mean_val_loss = np.mean(val_losses)
    print('Mean Validation Loss: {}'.format(mean_val_loss))
    return mean_val_loss

def test(sess, batch_generator, dataset, model):
    print('Testing')
    state = sess.run(model.initial_state)
    test_losses = []
    for batch in batch_generator:
        if dataset.epochs_completed == 1:
            dataset._epochs_completed = 0
            break
        x, y = batch.sources, batch.targets
        feed = {model.input_data: x, model.targets: y,
                        model.input_keep_prob: 1.0,
                        model.output_keep_prob: 1.0}
        for i, (c, h) in enumerate(model.initial_state):
            feed[c] = state[i].c
            feed[h] = state[i].h

        # instrument for tensorboard
        summ, val_loss, state, eval_step = sess.run([model.cost, model.final_state,
                                        model.eval_step_increment], feed)
        test_losses.append(val_loss)
    mean_test_loss = np.mean(test_losses)
    print('Test Loss: {}'.format(mean_test_loss))
    return mean_test_loss


if __name__ == '__main__':
    main()
