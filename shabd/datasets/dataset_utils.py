# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import os
import collections
import tflearn
import logging
import numpy as np
from tensorflow.contrib.learn.python.learn.datasets import base

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
    
class DataSet(object):

    def __init__(self, path, vocab, max_seq_len, nlp):

        self.path = path
        self.nlp = nlp
        self._epochs_completed = 0
        self._index_in_epoch = 0
        self.vocab_size = len(vocab[0])
        self.vocab_w2i = vocab[0]
        self.vocab_i2w = vocab[1]
        self.datafile = None
        self.max_seq_length = max_seq_len
        self.Batch = collections.namedtuple('Batch', ['sources', 'targets'])

    def open(self):
        self.datafile = open(self.path, 'r')

    def close(self):
        self.datafile.close()

    def padseq(self, data):
        if self.pad == 0:
            return data
        else:
            return tflearn.data_utils.pad_sequences(data, maxlen=self.pad,
                    dtype='int32', padding='post', truncating='post', value=1)

    def id2seq(self, data):
        buff = []
        for seq in data:
            w_seq = []
            for term in seq:
                if term in self.vocab_i2w:
                    if term == 0 or term == 1 or term == 2:
                        continue
                    w_seq.append(self.vocab_i2w[term])
            sent = ' '.join(w_seq)
            buff.append(sent)
        return buff

    def seq2id(self, data):
        buff = []
        for seq in data:
            id_seq = []
            for term in seq:
                if term in self.vocab_w2i:
                    id_seq.append(self.vocab_w2i[term])
                else:
                    id_seq.append(self.vocab_w2i['UNK'])
            buff.append(id_seq)
        return buff

    def next_batch(self, batch_size):
        if not self.datafile:
            raise Exception('The dataset needs to be open before being used. '
                            'Please call dataset.open() before calling '
                            'dataset.next_batch()')

        sources, targets, tokens = [], [], []
        while True:
            for line in self.datafile:
                line = line.strip()
                doc = self.nlp(line)
                for sent in doc.sents:
                    tokens.append('SEQUENCE_BEGIN')
                    for tok in sent:
                        if tok.text.strip() != '' and tok.ent_type_ != '':
                            tokens.append(tok.text)
                        else:
                            tokens.append(tok.text.lower())
                    tokens.append('SEQUENCE_END')
                if len(tokens) >= self.max_seq_length + 1:
                    sources.append(tokens[0: self.max_seq_length])
                    targets.append(tokens[1: self.max_seq_length+1])
                    tokens = tokens[self.max_seq_length:]
                if len(sources) == batch_size:
                    batch = self.Batch(
                        sources=np.array(self.seq2id(sources[:batch_size])),
                        targets=np.array(self.seq2id(targets[:batch_size])))
                    yield batch
                    sources, targets = [], []
            self._epochs_completed += 1
            self.datafile.seek(0)

    @property
    def epochs_completed(self):
        return self._epochs_completed


def load_dataset(data_dir, dataset, nlp, max_seq_len=50):

    dataset_root = os.path.join(data_dir, 'datasets', dataset)
    train_path = os.path.join(dataset_root, 'train', 'train.txt')
    dev_path = os.path.join(dataset_root, 'dev', 'dev.txt')
    test_path = os.path.join(dataset_root, 'test', 'test.txt')
    vocab_path = os.path.join(dataset_root, 'vocab.txt')
    metadata_path = os.path.join(dataset_root, 'metadata.txt')

    w2i, i2w = make_vocabulary(vocab_path)

    train = DataSet(train_path, (w2i, i2w), max_seq_len=max_seq_len, nlp=nlp)
    dev = DataSet(dev_path, (w2i, i2w), max_seq_len=max_seq_len, nlp=nlp)
    test = DataSet(test_path, (w2i, i2w), max_seq_len=max_seq_len, nlp=nlp)
    vocab_size = len(w2i)
    return base.Datasets(train=train, validation=dev, test=test), vocab_size, \
           vocab_path, w2i, i2w, metadata_path

def make_vocabulary(vocab_path):
    w2i = {}
    i2w = {}
    with open(vocab_path, 'r') as vf:
        wid = 0
        for line in vf:
            term = line.strip().split('\t')[0]
            w2i[term] = wid
            i2w[wid] = term
            wid += 1
    return w2i, i2w

def get_preloaded_w2v(data_dir, datasetname):
    dataset_root = os.path.join(data_dir, 'datasets', datasetname)
    w2v_path = os.path.join(dataset_root, 'w2v.npy')
    if os.path.exists(w2v_path):
        return np.load(w2v_path)
    else:
        return None

def find_ngrams(input_list, n):
    return zip(*[input_list[i:] for i in range(n)])

