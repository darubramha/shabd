import progressbar
import numpy as np
import argparse
import spacy

parser = argparse.ArgumentParser(
    description="Generating a numpy array of lreloaded w2v.")
parser.add_argument(
    "--vocab_path",
    dest="vocab_path",
    type=str,
    default='vocab.txt',
    help="The path to the vocab file.")
args = parser.parse_args()

nlp = spacy.load('en_core_web_md')

embedding = None
n_terms = 0

bar = progressbar.ProgressBar(max_value=progressbar.UnknownLength,
                              redirect_stdout=True)
with open(args.vocab_path, 'r') as vf:
    for term in vf:
        term = term.strip().split('\t')[0]
        tok = nlp(term)
        vec = tok.vector
        if embedding is not None:
            embedding = np.vstack((embedding, vec))
        else:
            embedding = np.expand_dims(vec, axis=0)
        n_terms += 1
        bar.update(n_terms)
bar.finish()

np.save('w2v', fix_imports=True, arr=embedding)

print('{} terms in the vocab and embedding shape {}'.format(n_terms,
                                                            embedding.shape))
