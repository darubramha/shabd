from __future__ import print_function

import argparse
import os

import spacy
import tensorflow as tf
from six.moves import cPickle

from shabd.models.rnn import Model

nlp = spacy.load('en_core_web_md')

def main():
    parser = argparse.ArgumentParser(
                       formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--save_dir', type=str, default='save',
                        help='model directory to store checkpointed models')
    args = parser.parse_args()
    sample(args)


def sample(args):
    with open(os.path.join(args.save_dir, 'config.pkl'), 'rb') as f:
        saved_args = cPickle.load(f)
    with open(os.path.join(args.save_dir, 'vocab.pkl'), 'rb') as f:
        w2i, i2w = cPickle.load(f)
    model = Model(saved_args, training=False)
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(args.save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            in_strings = ['I commanded the sleeves should be cut out and',
                          'bill, give me thy mete-yard, and spare not me',
                          'Fear you not him. Sirrah Biondello,']
            for in_string in in_strings:
                print(model.get_confidence(in_string, sess, i2w, w2i, nlp),
                      in_string)

if __name__ == '__main__':
    main()
