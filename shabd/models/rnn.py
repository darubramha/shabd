import tensorflow as tf
from tensorflow.contrib import rnn
from tensorflow.contrib import legacy_seq2seq
from shabd.datasets import dataset_utils as du
import numpy as np
from tensorflow.contrib.tensorboard.plugins import projector


class RNNModel():
    """
    An RNN bases Language model. This
    """
    def __init__(self, args, training=True):
        self.args = args
        self.w2vnp = du.get_preloaded_w2v(args.data_dir, args.dataset)
        self.global_step = tf.Variable(1, trainable=False, name='global_step')
        self.global_step_increment = tf.assign_add(self.global_step, 1,
                                                   name='global_step_increment')
        self.eval_step = tf.Variable(1, trainable=False, name='global_step')
        self.eval_step_increment = tf.assign_add(self.global_step, 1,
                                                   name='global_step_increment')
        self.input_keep_prob = tf.placeholder(tf.float32,
                                              name='input_keep_probab')
        self.output_keep_prob = tf.placeholder(tf.float32,
                                               name='output_keep_probab')
        if not training:
            args.batch_size = 1
            args.seq_length = 1

        if args.model == 'rnn':
            cell_fn = rnn.BasicRNNCell
        elif args.model == 'gru':
            cell_fn = rnn.GRUCell
        elif args.model == 'lstm':
            cell_fn = rnn.BasicLSTMCell
        elif args.model == 'nas':
            cell_fn = rnn.NASCell
        else:
            raise Exception("model type not supported: {}".format(args.model))

        cells = []
        for _ in range(args.num_layers):
            cell = cell_fn(args.rnn_size)
            if training and (args.output_keep_prob < 1.0 or args.input_keep_prob < 1.0):
                cell = rnn.DropoutWrapper(cell,
                                      input_keep_prob=self.input_keep_prob,
                                      output_keep_prob=self.output_keep_prob)
            cells.append(cell)

        self.cell = cell = rnn.MultiRNNCell(cells, state_is_tuple=True)

        self.input_data = tf.placeholder(
            tf.int32, [args.batch_size, args.seq_length])
        self.targets = tf.placeholder(
            tf.int32, [args.batch_size, args.seq_length])
        self.keep_prob = tf.placeholder(tf.float32)
        self.initial_state = cell.zero_state(args.batch_size, tf.float32)

        with tf.variable_scope('rnnlm'):
            softmax_w = tf.get_variable("softmax_w",
                                        [args.rnn_size, args.vocab_size])
            softmax_b = tf.get_variable("softmax_b", [args.vocab_size])

        if self.w2vnp is not None:
            w2v_init = tf.constant(self.w2vnp, dtype=tf.float32)
            self.embedding = tf.Variable(w2v_init, trainable=True,
                                      name="embedding")
        else:
            self.embedding = tf.get_variable("embedding", [args.vocab_size,
                                                           args.rnn_size])
        self.config = projector.ProjectorConfig()
        embedding = self.config.embeddings.add()
        embedding.tensor_name = self.embedding.name
        embedding.metadata_path = args.metadata_path

        inputs = tf.nn.embedding_lookup(self.embedding, self.input_data)

        # dropout beta testing: double check which one should affect next line
        if training and args.output_keep_prob:
            inputs = tf.nn.dropout(inputs, self.output_keep_prob)

        inputs = tf.split(inputs, args.seq_length, 1)
        inputs = [tf.squeeze(input_, [1]) for input_ in inputs]

        def loop(prev, _):
            prev = tf.matmul(prev, softmax_w) + softmax_b
            prev_symbol = tf.stop_gradient(tf.argmax(prev, 1))
            return tf.nn.embedding_lookup(self.embedding, prev_symbol)

        outputs, last_state = legacy_seq2seq.rnn_decoder(inputs,
                                self.initial_state, cell,
                                loop_function=loop if not training else None,
                                scope='rnnlm')
        output = tf.reshape(tf.concat(outputs, 1), [-1, args.rnn_size])


        self.logits = tf.matmul(output, softmax_w) + softmax_b
        self.probs = tf.nn.softmax(self.logits)
        loss = legacy_seq2seq.sequence_loss_by_example(
                [self.logits],
                [tf.reshape(self.targets, [-1])],
                [tf.ones([args.batch_size * args.seq_length])])
        self.cost = tf.reduce_sum(loss) / args.batch_size / args.seq_length
        with tf.name_scope('cost'):
            self.cost = tf.reduce_sum(loss) / args.batch_size / args.seq_length
        self.final_state = last_state
        self.lr = tf.Variable(0.0, trainable=False)
        tvars = tf.trainable_variables()
        grads, _ = tf.clip_by_global_norm(tf.gradients(self.cost, tvars),
                args.grad_clip)
        with tf.name_scope('optimizer'):
            optimizer = tf.train.AdamOptimizer(self.lr)
        self.train_op = optimizer.apply_gradients(zip(grads, tvars))

        # instrument tensorboard
        tf.summary.histogram('logits', self.logits)
        tf.summary.histogram('loss', loss)
        tf.summary.scalar('loss', self.cost)

    def sample(self, sess, i2w, w2i, nlp, num=200, prime='The ',
               sampling_type=1):
        state = sess.run(self.cell.zero_state(1, tf.float32))
        prime_doc = nlp(prime)
        in_string = ''
        for token in prime_doc:
            x = np.zeros((1, 1))
            in_string = token.text
            if token.ent_type_ == '':
                in_string = in_string.lower()
            if in_string in w2i:
                x[0, 0] = w2i[in_string]
            else:
                x[0, 0] = w2i['UNK']

            feed = {self.input_data: x, self.initial_state: state}
            [state] = sess.run([self.final_state], feed)

        def weighted_pick(weights):
            t = np.cumsum(weights)
            s = np.sum(weights)
            return(int(np.searchsorted(t, np.random.rand(1)*s)))

        ret = prime
        for n in range(num):
            x = np.zeros((1, 1))
            x[0, 0] = w2i[in_string]
            feed = {self.input_data: x, self.initial_state: state}
            [probs, state] = sess.run([self.probs, self.final_state], feed)
            p = probs[0]

            if sampling_type == 0:
                sample = np.argmax(p)
            elif sampling_type == 2:
                if in_string == ' ':
                    sample = weighted_pick(p)
                else:
                    sample = np.argmax(p)
            else:  # sampling_type == 1 default:
                sample = weighted_pick(p)

            pred = i2w[sample]
            ret += ' ' + pred
            in_string = pred
        return ret

    def get_confidence(self, in_string, sess, w2i, nlp):
        state = sess.run(self.cell.zero_state(1, tf.float32))
        prime_doc = nlp(in_string)
        tokens = []
        for token in prime_doc:
            in_string = token.text
            if token.ent_type_ == '':
                in_string = in_string.lower()
            tokens.append(in_string)
        #tokens.append('SEQUENCE_END')

        id_seq = []
        for token in enumerate(tokens):
            if token in w2i:
                id_seq.append(w2i[token])
            else:
                id_seq.append(w2i['UNK'])
        confidence = []
        for curr, id in enumerate(id_seq):
            next = curr + 1
            x = np.zeros((1, 1))
            x[0, 0] = id
            feed = {self.input_data: x, self.initial_state: state}
            [probs, state] = sess.run([self.probs, self.final_state], feed)
            p = probs[0]
            if next < len(id_seq):
                next_id = id_seq[next]
                confidence.append(p[next_id])
            else:
                break
        print(len(confidence))
        abs_conf = np.sum(confidence)
        exp_conf = np.exp(abs_conf)
        return abs_conf, exp_conf